# Installation
## Requirements
PHP 7.1.х , Composer 1.4.х 

git clone https://vdemeshko@bitbucket.org/vdemeshko/disc_test.git

php composer install

php artisan migrate:refresh --seed

php phpunit (optional to run tests)

web server entry point: /public


## documentation:
### Product API
REST resource /api/product/{?product_id} [DELETE, UPDATE, PATCH, PUT, POST, GET]

to add a voucher to the product: /api/product/{product_id}/voucher/{voucher_id} [PATCH]

to delete a voucher from the product: /api/product/{product_id}/voucher/{voucher_id} [DELETE]

to buy a product: /api/product/{product_id}/buy  [PATCH]

### Voucher API
REST resource /api/voucher/{?voucher_id} [DELETE, UPDATE, PATCH, PUT, POST, GET]

to add a product to the voucher: /api/voucher/{voucher_id}/product/{product_id} [PATCH]

to delete a product from the voucher: /api/voucher/{voucher_id}/product/{product_id} [DELETE]