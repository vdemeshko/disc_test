<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Models\Discount::class, function (Faker\Generator $faker) {
    $tiers = [10, 15, 20, 25];

    return [
        'value' => $faker->unique()->randomElement($tiers)
    ];
});

$factory->define(App\Models\Voucher::class, function (Faker\Generator $faker) {
    $discIds = [1, 2, 3, 4];

    return [
        'discount_id' => $faker->randomElement($discIds),
        'started_at' => $faker->dateTimeBetween('-1 week', '-1 day'),
        'ended_at' => $faker->dateTimeBetween('+1 week', '+1 month')
    ];
});

$factory->define(App\Models\Product::class, function (Faker\Generator $faker) {

    return [
        'name' => 'Product ' . $faker->unique()->uuid,
        'price' => $faker->randomFloat($nbMaxDecimals = NULL, $min = 0, $max = 1000)
    ];
});