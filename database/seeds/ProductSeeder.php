<?php

use App\Models\Product;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        factory(\App\Models\Product::class, 100)->create()->each( function ($product) use ($faker){
            $iterations = $faker->randomElement([1,2,3,4]);
            $vouchersIds = [];
            for ($i = 0; $i <=  $iterations; $i++) {
                $vouchersIds[] = $faker->numberBetween(1, 99);
            }
            /** @var Product $product */
            $product->vouchers()->sync($vouchersIds);
        });
    }
}
