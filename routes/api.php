<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**
 * Product API routes
 */
Route::patch('product/{product}/voucher/{voucher}', 'Api\ProductController@addVoucher');
Route::delete('product/{product}/voucher/{voucher}', 'Api\ProductController@deleteVoucher');
Route::patch('product/{product}/buy', 'Api\ProductController@buyProduct');
Route::resource('product', 'Api\ProductController', ['only' => ['index', 'show', 'store', 'delete']]);

/**
 * Voucher API routes
 */
Route::patch('voucher/{voucher}/product/{product}', 'Api\VoucherController@addProduct');
Route::delete('voucher/{voucher}/product/{product}', 'Api\VoucherController@deleteProduct');
Route::resource('voucher', 'Api\VoucherController', ['only' => ['index', 'show', 'store', 'delete']]);