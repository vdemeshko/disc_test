<?php

namespace Tests\Feature;


use Tests\TestCase;

class ApiTest extends TestCase
{
    public function testAProductIndexRoute()
    {
        $response = $this->json('get', '/api/product');
        $response->assertStatus(200);
    }

    public function testAProductStoreRoute()
    {
        $response = $this->json('post', '/api/product');
        $response->assertStatus(422);
    }

    public function testAProductIndexRouteHasAJsonResponse()
    {
        $response = $this->json('get', '/api/product');
        $response
            ->assertStatus(200)
            ->assertJsonStructure();

    }

    public function testAProductIndexHasAJsonResponseWithAProductDataArray()
    {
        $response = $this->json('get', '/api/product');
        $response
            ->assertStatus(200)
            ->assertJsonFragment([
                'data'
            ]);
    }

    public function testAProductShowRouteHasAJsonResponseWithGivenId()
    {
        $response = $this->json('get', '/api/product/1');
        $response
            ->assertStatus(200)
            ->assertJsonFragment([
                'id' => 1
            ]);
    }

    public function testAVoucherIndexRoute()
    {
        $response = $this->json('get', '/api/voucher');
        $response->assertStatus(200);
    }

    public function testAVoucherStoreRoute()
    {
        $response = $this->json('post', '/api/voucher');
        $response->assertStatus(422);
    }

    public function testAVoucherIndexRouteHasAJsonResponse()
    {
        $response = $this->json('get', '/api/voucher');
        $response
            ->assertStatus(200)
            ->assertJsonStructure();

    }

    public function testAVoucherIndexHasAJsonResponseWithAProductDataArray()
    {
        $response = $this->json('get', '/api/voucher');
        $response
            ->assertStatus(200)
            ->assertJsonFragment([
                'data'
            ]);
    }

    public function testAVoucherShowRouteHasAJsonResponseWithGivenId()
    {
        $response = $this->json('get', '/api/voucher/1');
        $response
            ->assertStatus(200)
            ->assertJsonFragment([
                'id' => 1
            ]);
    }
}
