<?php
/**
 * Created by PhpStorm.
 * User: slavik
 * Date: 2017-07-07
 * Time: 09:57
 */

namespace Tests\Unit;

use App\Models\Product;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ProductTest extends TestCase
{
    function testAProductHasAName()
    {
        $product = new Product(['name' => 'abc']);

        $this->assertEquals('abc', $product->name);
    }

    function testAProductHasAPrice()
    {
        $product = new Product(['price' => 100]);

        $this->assertEquals(100, $product->price, '', 0.0001);
    }

    function testAProductHasAnAvailability()
    {
        $product = new Product(['available' => 0]);

        $this->assertEquals(0, $product->availible);
    }
}