<?php
/**
 * Created by PhpStorm.
 * User: slavik
 * Date: 2017-07-07
 * Time: 10:18
 */

namespace Tests\Unit;

use App\Models\Product;
use App\Models\Voucher;
use Carbon\Carbon;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class VoucherTest extends TestCase
{
    function testAVoucherHasAStartDate()
    {
        $now = Carbon::now();
        $product = new Voucher(['started_at' => $now]);

        $this->assertEquals($now->getTimestamp(), $product->started_at->getTimestamp(), '', 5);
    }

    function testAVoucherHasAEndDate()
    {
        $now = Carbon::now();
        $product = new Voucher(['ended_at' => $now]);

        $this->assertEquals($now->getTimestamp(), $product->ended_at->getTimestamp(), '', 5);
    }

    function testAVoucherHasAnActivity()
    {
        $product = new Product(['active' => 0]);

        $this->assertEquals(0, $product->active);
    }
}