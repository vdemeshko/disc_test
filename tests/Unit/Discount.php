<?php
/**
 * Created by PhpStorm.
 * User: slavik
 * Date: 2017-07-07
 * Time: 10:28
 */

namespace Tests\Unit;


use Tests\TestCase;

class Discount extends TestCase
{
    function testADiscountHasAValue()
    {
        $dicount = new Discount(['value' => 10]);

        $this->assertEquals(10, $dicount->value);
    }
}