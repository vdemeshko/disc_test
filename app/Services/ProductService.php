<?php

namespace App\Services;

use App\Http\Requests\ProductRequest;
use App\Http\Requests\ProductUpdateRequest;
use App\Models\Product;


/**
 *
 * Service layer for Product logic
 *
 * Class ProductService
 * @package App\Services
 */
class ProductService
{
    /**
     * @param ProductRequest $request
     * @return Product
     */
    public function create(ProductRequest $request): Product
    {
        return Product::create($request->all());

    }

    /**
     * @param ProductRequest $request
     * @param Product $product
     */
    public function update(ProductRequest $request, Product $product): void
    {
        $product->update($request->all());
    }

    /**
     * @param Product $product
     */
    public function buy(Product $product): void
    {
        $product->update(['available' => false]);
        $product->vouchers()->rawUpdate(['active' => false]);
    }

    /**
     * @param ProductRequest|ProductUpdateRequest $request
     * @param Product $product
     * @internal param $ProductRequest
     */
    public function sync($request, Product $product): void
    {
        $vouchers = $request->get('vouchers');
        if ($vouchers) {
            $product->vouchers()->sync($vouchers);
        }
    }

}