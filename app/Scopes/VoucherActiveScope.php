<?php

namespace App\Scopes;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

/**
 * Class VoucherActiveScope
 * @package App\Scopes
 */
class VoucherActiveScope implements Scope
{

    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder $builder
     * @param  \Illuminate\Database\Eloquent\Model $model
     * @return \Illuminate\Database\Eloquent\Builder $builder
     */
    public function apply(Builder $builder, Model $model)
    {
        $now = Carbon::now();
        return $builder
            ->whereDate('started_at', '<=', $now)
            ->whereDate('ended_at', '>=', $now)
            ->where('active', '=', true);
    }
}