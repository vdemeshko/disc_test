<?php
/**
 * Created by PhpStorm.
 * User: slavik
 * Date: 2017-07-05
 * Time: 21:56
 */

namespace App\Scopes;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

/**
 * Class ProductAvailabilityScope
 * @package App\Scopes
 */
class ProductAvailabilityScope implements Scope
{

    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder $builder
     * @param  \Illuminate\Database\Eloquent\Model $model
     * @return \Illuminate\Database\Eloquent\Builder $builder
     */
    public function apply(Builder $builder, Model $model)
    {
        return $builder->where('available', '=', true);
    }
}