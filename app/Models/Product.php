<?php

namespace App\Models;

use App\Models\Filters\Filterable;
use App\Scopes\ProductAvailabilityScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Class Product
 * @package App\Models
 */
class Product extends Model
{
    use Filterable;

    /**
     *  Maximum allowed discount
     */
    const MAXIMUM_DISCOUNT = 60;

    /**
     * Guarded properties for mass assignment
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * Calculated properties
     *
     * @var array
     */
    protected $appends = ['price_with_discount'];

    /**
     *  Eager models loading
     *
     * @var array
     */
    protected $with = ['vouchers'];

    public $timestamps = false;

    /**
     * Global model scope for available products
     */
    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new ProductAvailabilityScope());
    }

    /**
     * @return BelongsToMany
     */
    public function vouchers(): BelongsToMany
    {
        return $this->belongsToMany(Voucher::class, 'product_voucher');
    }

    /**
     * @return int
     */
    public function getTotalDiscount(): int
    {
        $totalDiscount = $this->calculateDiscount();
        return ($totalDiscount > self::MAXIMUM_DISCOUNT) ? self::MAXIMUM_DISCOUNT : $totalDiscount;
    }

    /**
     * @return int
     */
    private function calculateDiscount(): int
    {
        $discountSum = $this->vouchers->filter(function (Voucher $voucher) {
            return $voucher->isActive();
        })->sum('discount_value');
        return $discountSum;
    }

    /**
     * @return int
     */
    public function getPriceWithDiscountAttribute(): int
    {
        $discount = $this->getTotalDiscount();
        return ($discount > 0) ? $this->price - ($this->price * ($discount / 100)) : $this->price;
    }
}
