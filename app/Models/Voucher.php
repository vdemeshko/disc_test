<?php

namespace App\Models;

use App\Models\Filters\Filterable;
use App\Scopes\VoucherActiveScope;
use Carbon\Carbon;
use Faker\Provider\DateTime;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Class Voucher
 * @package App\Models
 */
class Voucher extends Model
{
    use Filterable;

    protected $guarded = ['id'];

    protected $with = ['discount'];

    protected $appends = ['discount_value'];

    protected $dates = ['started_at', 'ended_at'];

    public $timestamps = false;

    /**
     * Global model scope for active vouchers
     */
    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new VoucherActiveScope());
    }

    /**
     * @return BelongsToMany
     */
    public function products(): BelongsToMany
    {
        return $this->belongsToMany(Product::class, 'product_voucher');
    }

    /**
     * @return BelongsTo
     */
    public function discount(): BelongsTo
    {
        return $this->belongsTo(Discount::class);
    }

    /**
     * @return mixed
     */
    public function getDiscountValueAttribute()
    {
        return $this->discount->value;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        $now = Carbon::now();
        return $this->started_at->lte($now) && $this->ended_at->gte($now) && $this->active;
    }
}
