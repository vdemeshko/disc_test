<?php
/**
 * Created by PhpStorm.
 * User: slavik
 * Date: 2017-07-07
 * Time: 08:29
 */

namespace App\Models\Filters;

use Illuminate\Database\Eloquent\Builder;

class ProductFilters extends QueryFilters
{

    /**
     * Filter by id.
     *
     * @param  string $order
     * @return Builder
     */
    public function id($order = 'desc')
    {
        return $this->builder->orderBy('id', $order);
    }

    /**
     * Filter by price.
     *
     * @param  string $order
     * @return Builder
     */
    public function name($order = 'asc')
    {
        return $this->builder->orderBy('name', $order);
    }

    /**
     * Filter by name.
     *
     * @param  string $order
     * @return Builder
     */
    public function price($order = 'asc')
    {
        return $this->builder->orderBy('price', $order);
    }
}