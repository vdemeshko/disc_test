<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\VoucherRequest;
use App\Models\Voucher;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;


/**
 * Class VoucherController
 * @package App\Http\Controllers\Api
 */
class VoucherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return LengthAwarePaginator
     */
    public function index(Request $request): LengthAwarePaginator
    {
        return Voucher::paginate();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param VoucherRequest|Request $request
     * @return Voucher
     */
    public function store(VoucherRequest $request): Voucher
    {
        $voucher = Voucher::create($request->all());
        return $voucher;
    }

    /**
     * Display the specified resource.
     *
     * @param Voucher $voucher
     * @return Voucher
     * @internal param int $id
     */
    public function show(Voucher $voucher): Voucher
    {
        return $voucher;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param Voucher $voucher
     * @return Voucher
     * @internal param int $id
     */
    public function update(Request $request, Voucher $voucher): Voucher
    {
        $voucher->update($request->all());
        return $voucher->fresh();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Voucher $voucher
     * @return Voucher
     * @internal param int $id
     */
    public function destroy(Voucher $voucher): Voucher
    {
        $voucher->delete();
        return $voucher;
    }

    /**
     * Add product
     *
     * @param Voucher $voucher
     * @param Product
     * @return Voucher
     */
    public function addProduct(Voucher $voucher, Product $product): Voucher
    {
        $voucher->products()->attach($product->id);
        return Voucher::find($product->id);
    }

    /**
     * Delete product
     *
     * @param Voucher $voucher
     * @param Product $product
     * @return Voucher
     */
    public function deleteProduct(Voucher $voucher, Product $product): Voucher
    {
        $voucher->products()->detach($product->id);
        return Voucher::find($product->id);
    }
}
