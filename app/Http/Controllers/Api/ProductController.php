<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\ProductRequest;
use App\Http\Requests\ProductUpdateRequest;
use App\Models\Filters\ProductFilters;
use App\Services\ProductService;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Voucher;


/**
 * Class ProductController
 * @package App\Http\Controllers\Api
 */
class ProductController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param ProductFilters $filters
     * @return LengthAwarePaginator
     * @internal param Request $request
     */
    public function index(ProductFilters $filters): LengthAwarePaginator
    {
        return Product::filter($filters)->paginate();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ProductService $service
     * @param ProductRequest $request
     * @return Product
     * @internal param $ProductRequest
     */
    public function store(ProductService $service, ProductRequest $request): Product
    {
        $product = $service->create($request);
        $service->sync($request, $product);
        return $product;
    }

    /**
     * Display the specified resource.
     *
     * @param Product $product
     * @return Product
     * @internal param int $id
     */
    public function show(Product $product): Product
    {
        return $product;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ProductService $service
     * @param ProductUpdateRequest|Request $request
     * @param Product $product
     * @return Product
     * @internal param $
     * @internal param int $id
     */
    public function update(ProductService $service, ProductUpdateRequest $request, Product $product): Product
    {
        $product->update($request->all());
        $service->sync($request, $product);
        return $product->fresh();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Product $product
     * @return Product
     * @internal param int $id
     */
    public function destroy(Product $product): Product
    {
        $product->delete();
        return $product;
    }

    /**
     * @param Product $product
     * @param Voucher $voucher
     * @return Product
     */
    public function addVoucher(Product $product, Voucher $voucher): Product
    {
        $product->vouchers()->attach($voucher->id);
        return Product::find($product->id);
    }

    /**
     * @param Product $product
     * @param Voucher $voucher
     * @return Product
     */
    public function deleteVoucher(Product $product, Voucher $voucher): Product
    {
        $product->vouchers()->detach($voucher->id);
        return Product::find($product->id);
    }

    /**
     * @param ProductService $service
     * @param Product $product
     * @return Product
     */
    public function buyProduct(ProductService $service, Product $product): Product
    {
        $service->buy($product);
        return Product::find($product->id);
    }

}
